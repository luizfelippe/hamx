## hammurabi X tutorials

(the tutorials serve as both user instructions and precision verifications)

- Tutorial 01, presents the XML usage and essential parameters for a hammurabi simulation run.
It also validates and explains the methods for multi-shell masking.

- Tutorial e01, presents a fast precision check for the LoS integration.

- Tutorial e02, presents a fast precision check for numerical CRE I/O.

- Tutorial e03, presents a fast precision check for the random magnetic field generator.

- some mathematical materials can be found in [hammurabi X: Simulating Galactic Synchrotron Emission with Random Magnetic Fields](https://arxiv.org/abs/1907.00207)

- note that in the tutorials we import the python wrapper Hampyx without installation, please check the [**wiki page**](https://bitbucket.org/hammurabicode/hamx/wiki/Home) for instructions.
